const typeDefs = `

type Product{    
    id: ID!
    category: String
    sale_end: String
    sale_start: String
    image: String
    name: String
    offer_price: Int  
    price: Int
    quantity: Int   
    removed: Boolean
    outstocked: Boolean
}

type Edge{
    node: Product
    cursor: String
}

type PageInfo {
    startCursor: String
    hasNextPage: Boolean
}

type ProductResult {
    totalCount: String
    edges: [Edge]
    pageInfo: PageInfo
}


type Order {
    id: ID!
    status: String
    deliver: Boolean
    disbursed: Boolean
    disburseDate: String
    packed: Boolean
    payment: Payment
    products: [OrderProduct]
    customer: Customer
    createdAt: String
    updatedAt: String
}

type Payment {  
    ref: String
    timestamp: String
    amount: String
}

type OrderProduct {
    id: ID
    meta: Product
    quantity: Int
}

type Customer {
    id:ID!
    displayName: String
    email: String
    location: String
    phoneNumber: String
    photo: String

    cart: Order
    old: [Order]
    packed: [Order]
    awaitingPacking: [Order]
}

type Admin {
    id: ID!
    username: String
    password: String
    telephone: String
    removed: Boolean
}

type Query { 
    getProducts(first:Int , afterCursor: String , category: String): ProductResult
    getCustomer(id:ID!)  : Customer  
    getAllProducts: [Product]   
    getCustomers : [Customer]
    getOrders : [Order]
    getAdmins : [Admin]
}

type Mutation {
    addAdmin (
        username: String
        password: String
        telephone: String
    ): Admin

    removeAdmin (
         id: ID!
    ): Admin

    changeAdminPassword(
        id: ID!
        password:String!
    ):
    Admin

    addProduct (
        category: String
        sale_end: String
        sale_start: String
        image: String!
        name: String
        offer_price: Int
        on_offer: Boolean
        price: Int
        quantity: Int
        searchableKeyword: [String]
    ) : Product
    addCustomer (
        displayName: String,
        email: String,
        location: String,
        phoneNumber: String,
        photo: String,
    ) : Customer
    updateCustomer (
        id: ID! ,  
        displayName: String,
        email: String,
        location: String,
        phoneNumber: String,
        photo: String
    ) : Customer   
      
    updateProduct (
        id: ID!
        category: String
        sale_end: String
        sale_start: String
        image: String
        name: String
        offer_price: Int      
        price: Int
        outstocked: Boolean
        quantity: Int
        removed: Boolean        
    ) : Product


    addToCart( 
        customer: ID!
        prod_id: ID!    
        quantity: Int
    ): Order

    updateCart( 
        action: String!
        prod_id: ID!
        customer: ID!
    ): Order

    updateOrder(
        id:ID!
        status: String
        deliver: Boolean
        disbursed: Boolean
        disburseDate: String
        packed: Boolean
        ref: String
        timestamp: String
        amount: String
        outstocked: Boolean
    ): Order
}

`;

export default typeDefs;
