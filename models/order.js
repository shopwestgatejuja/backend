import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"

const { Schema } = mongoose

export const OrderSchema = new Schema(
  {
    status: String,
    deliver: { type: Boolean, default: false },
    disbursed: { type: Boolean, default: false },
    disburseDate: String,
    packed: { type: Boolean, default: false },
    payment: {
      ref: String,
      timestamp: String,
      amount: String,
    },
    products: [
      {
        prod_id: Schema.Types.ObjectId,
        quantity: Number,
      },
    ],
    customer: { type: Schema.Types.ObjectId, ref: "Customer" },
  },
  {
    collection: "orders",
  }
)

OrderSchema.plugin(timestamps)

OrderSchema.index({ createdAt: 1, updatedAt: 1 })

export const Order = mongoose.model("Order", OrderSchema)
