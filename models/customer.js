import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"

const { Schema } = mongoose

export const CustomerSchema = new Schema(
  {
    displayName: String,
    email: String,
    location: String,
    phoneNumber: String,
    photo: String,
  },
  {
    collection: "customers",
  }
)
CustomerSchema.plugin(timestamps)

CustomerSchema.index({ createdAt: 1, updatedAt: 1 })

export const Customer = mongoose.model("Customer", CustomerSchema)
